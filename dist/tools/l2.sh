#! /bin/sh
# -*- coding: utf-8; indent-tabs-mode: nil; -*-
# -*- eval: (set-language-environment Russian) -*-
# Time-stamp: <2021-11-14 11:07:29 roukoru>

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
#set -euo pipefail
#IFS=$'\n\t'

WINE_LUTRIS="/opt/lutris"
WINE_GENTOO="/usr"
WINE_PATH="${WINE_GENTOO}"
# WINE="${WINE_PATH}/bin/wine"
# WINECFG="${WINEPATH}/bin/winecfg"

ROOTDIR="/mnt/WCATR9679470"
GAMEDIR="${ROOTDIR}/games" # wine drive f:
L2VAR="l2h5p5-L2Age" # l2 variant

function installDXVK() {
    WINEPREFIX="${1}/wine/wine_4.0-${2}" setup_dxvk-bin.sh install --symlink
}

function uninstallDXVK() {
    WINEPREFIX="${1}/wine/wine_4.0-${2}" setup_dxvk-bin.sh uninstall
}

function do_wine_config() {
    WINEPREFIX="${1}/wine/wine_4.0-${2}" "${WINECFG}"
}

function do_wine_regedit() {
    WINEPREFIX="${1}/wine/wine_4.0-${2}" "${WINE}" regedit
}

function check_wine_version() {
    v=`wine --version`
    v="${v:5}"
    if [[ "${v}" != "${WINE_STABLE}" ]]; then
        printf "wine is not wine-stable, exiting.\n"
        printf "WINEPREFIX %s\n" "${WINEPREFIX}"
        printf "wine %s\n" "${v}"
        exit 1
    fi
}

#
# --------[ hard lndir example ]--------
#

function l2lndir() {
    L2MASTER="l2h5p5-L2Age.u01"
    L2CLONE="$1"

    mkdir "${L2CLONE}"
    cd "${L2CLONE}"

    # l2 sub dirs
    mkdir ANIMATIONS FORCEFEEDBACK L2TEXT MAPS MUSIC REPLAY SCREENSHOT \
          SOUNDS STATICMESHES SYSTEM SYSTEXTURES TEXTURES VOICE

    # l2 instance local files
    # CheckGrp.log L2CompiledShader.bin l2.log OPTION.INI s_info.ini
    for f in CHATFILTER.INI CHECKGRP.LOG L2.LOG L2COMPILEDSHADER.BIN \
             OPTION.INI S_INFO.INI USER.INI WINDOWSINFO.INI ; do
        cp "../${L2MASTER}/SYSTEM/${f}" SYSTEM
    done

    for d in ANIMATIONS FORCEFEEDBACK L2TEXT MAPS MUSIC SOUNDS \
             STATICMESHES SYSTEM SYSTEXTURES TEXTURES VOICE ; do
        pushd $d
        find "../../${L2MASTER}/${d}/" -type f -exec ln \{\} \;
        popd
    done

    ln "../${L2MASTER}/LINEAGEII.EXE"
    ln "../${L2MASTER}/PATCHW32.DLL"
}

# --------------------------------

show_help() {
    printf -- "-%s\t%s\n" "d" "dxvk install"
    printf -- "-%s\t%s\n" "D" "dxvk uninstall"
    printf -- "-%s\t%s\n" "uN" "use wine profile N\n"
    printf -- "-%s\t%s\n" "l" "run with lutris"
    printf -- "-%s\t%s\n" "g" "run with gentoo wine"
    printf -- "-%s\t%s\n" "c" "exec winecfg"
    printf -- "-%s\t%s\n" "r" "exec wine regedit"
    printf -- "-%s\t%s\n" "t" "exec winetricks"
    printf -- "-%s\t%s\n" "v" "show env variables"
    printf -- "-%s\t%s\n" "h" "this help"
}

parse() {
    while getopts 'hrcdDlgu:vt' c ; do
        case ${c} in
            u)
                l2user=${OPTARG}
                ;;
            d)
                run_Wine="DXVKinstall"
                ;;
            D)
                run_Wine="DXVKuninstall"
                ;;
            c)
                run_Wine="config"
                ;;
            r)
                run_Wine="regedit"
                ;;
            t)
                run_Wine="winetricks"
                ;;
            v)
                run_Wine="variables"
                ;;
            l)
                WINE_PATH="${WINE_LUTRIS}"
                ;;
            g)
                WINE_PATH="${WINE_GENTOO}"
                ;;
            h)
                show_help
                exit 1
                ;;
            *)
                printf "%s\n" "${arg}"
                show_help
                exit 1
                ;;
        esac
    done
}

run_Wine=
l2user=1
parse "$@"

WINE="${WINE_PATH}/bin/wine"
if [ ! -x "${WINE}" ]; then
    printf "error: not executable wine at '${WINE}'\n"
    exit 1
fi

WINEPREFIX=`printf "${ROOTDIR}/wine/wine_4.0-%02i" "${l2user}"`
if [ ! -d "${WINEPREFIX}" ]; then
    printf "error: not exist or readable wine home: '${WINEPREFIX}'\n"
    exit 2
fi
export WINEPREFIX

L2EXE=`printf "${L2VAR}.u%02i/SYSTEM/L2.EXE" "${l2user}"`

case ${run_Wine} in
    variables)
        printf "WINE         : %s\n" "${WINE}"
        printf "WINEPREFIX   : %s\n" "${WINEPREFIX}"
        printf "L2EXE        : %s\n" "${GAMEDIR}/${L2EXE}"
        printf "wine version : "
        exec ${WINE} --version
        ;;
    DXVKinstall)
        exec setup_dxvk-bin.sh install --symlink
        ;;
    DXVKuninstall)
        exec setup_dxvk-bin.sh uninstall
        ;;
    config)
        exec ${WINE_PATH}/bin/winecfg
        ;;
    regedit)
        exec ${WINE} regedit
        ;;
    winetricks)
        exec winetricks
        ;;
    *)
        ;;
esac

if [ ! -r "${GAMEDIR}/${L2EXE}" ]; then
    printf "error: not exist or readable L2 executable: '${L2EXE}'\n"
    exit 3
fi

L2PID=`pgrep -f "${L2EXE}"`
if [ "foo${L2PID}" != "foo" ]; then
    printf "L2 from .u${l2user} already started!\n"
    exit 4
fi

export WINEDEBUG="-all"
export __GL_THREADED_OPTIMIZATIONS=1

set -x
cd "${GAMEDIR}"
# exec "${WINE}" "${L2EXE}"
"${WINE}" "${L2EXE}"
